/**
 * Created by dhanalakshmi on 2/9/17.
 */

var pageApp = angular.module('pageApp', ['ui.router']);


pageApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/admin');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================

    .state('admin', {
        url:"/admin",
        templateUrl:'credentials/admin.html'
    })
    .state('admin.signIn', {
        url: "/signIn",
        templateUrl: 'credentials/signIn.html',
        controller: 'loginCtrl'
    })
    .state('admin.signUp', {
        url:"/signUp",
        templateUrl: 'credentials/signUp.html',
        controller: 'loginCtrl'
    })
        .state('dashboard', {
            url: "/dashboard",
            templateUrl:'template/dashboard.html'
        })
});



