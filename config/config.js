var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'insertApplication'
    },
    port: 5001,
    db:'mongodb://localhost/chat-app'

  },

  test: {
    root: rootPath,
    app: {
      name: 'insertApplication'
    },
    port: 5001,
    db: 'mongodb://localhost/chat-app'
  },

/*
  production: {
    root: rootPath,
    app: {
      name: 'insertApplication'
    },
    port: 5001,
    db: 'mongodb://development:devPassw0rd@ds051990.mongolab.com:51990/bookish'
  }*/



};

module.exports = config[env];
